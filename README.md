# CountOps
Simple LLVM analysis pass to determine number of operations in a file
## Install
### Build dependencies
- [Ninja](https://ninja-build.org/)
- [CMake](https://cmake.org/)
- [LLVM](https://github.com/llvm-mirror/llvm) - Download the source code only. Build not necessary
### Recommended directory structure
```
TOPLEVELDIR
|- llvm
|- CountOps
```

## Run
### Settings
1. Open file CountOps/runMe
2. Set variable LLVMHOME to your llvm source directory. If you've followed recommended directory structure it will be - TOPLEVELDIR/llvm

### Run CountOps
```bash
cd TOPLEVELDIR/CountOps/
./runMe test/foo.c
```
You can use pass any number of C/C++ files to the script. The llvm code is generated in the same directory as C file
